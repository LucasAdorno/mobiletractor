import React, { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SmsListener from 'react-native-android-sms-listener'
import api from './services/api';

export default function App() {
 
  useEffect(() => {
    SmsListener.addListener(message => {
      
      let breakedMessage = message.body.split(', ');
      
      const data = {
        id: breakedMessage[0],
        lat: breakedMessage[1],
        long: breakedMessage[2],
        hour: breakedMessage[3],
        date: breakedMessage[4]
      };
      api.post('users', data);
      alert(`Enviado para a api!`);
    })
  }, [])

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Reading Sms's</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00ddcc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 22,
    textAlign: 'center',
    color: '#fafafa'
  }
});
